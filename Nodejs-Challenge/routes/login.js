var express = require('express');
var router = express.Router();
const data = require('../data.json');
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended: false}));

router.post('/', function(req, res, next) {
  const username = req.body.username;
  const password = req.body.password;
  
  const dataUsername = data[0].username;
  const dataPassword = data[0].password;

  if(dataUsername === username && dataPassword === password){
      return res.send('Berhasil Login')
  }
  return res.send('Username atau Password Anda Salah')

});

module.exports = router;
