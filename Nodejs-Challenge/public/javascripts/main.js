    const _batuCom = document.getElementById('batu-com');
    const _kertasCom = document.getElementById('kertas-com');
    const _guntingCom = document.getElementById('gunting-com');
    

    const _batuPlayer = document.getElementById('batu-player');
    const _kertasPlayer = document.getElementById('kertas-player');
    const _guntingPlayer = document.getElementById('gunting-player');
   

// pilihan computer
function getPilihanComputer(){
    const comBrain = Math.floor(Math.random()*3);
    if ( comBrain < 1) return 'batu-player';
    if ( comBrain > 1) return 'gunting-player';
    return 'kertas-player'; 
}

// hasil menang & kalah
function getHasil(playerPick, comBrain){
    if( playerPick == comBrain ) return 'DRAW';
    if( playerPick == 'batu-player') return ( comBrain == 'gunting-player' )? 'PLAYER 1 WIN': 'COM WIN';
    if( playerPick == 'kertas-player') return ( comBrain == 'batu-player' )? 'PLAYER 1 WIN': 'COM WIN';
    if( playerPick == 'gunting-player' ) return ( comBrain == 'kertas-player' )? 'PLAYER 1 WIN': 'COM WIN';    
}

// pilihan player
const pilihanPlayerAll = document.querySelectorAll('.col-player img');
pilihanPlayerAll.forEach(function(pil){
   pil.addEventListener('click',function(){
    const pilihanPlayer = pil.id;
    console.log('player:',pilihanPlayer);
    const pilihanComputer = getPilihanComputer();
    console.log('Com:',pilihanComputer);
    const hasil = getHasil(pilihanPlayer, pilihanComputer);
    console.log('hasil:',hasil);

    const _cardwin = document.getElementById('cardpwin');
    const _vs = document.getElementById('vs');
    _cardwin.innerHTML = hasil; 
    _cardwin.style.visibility = 'visible';
    _vs.style.visibility = 'hidden';

    const _colbatu = document.querySelector('.col-batu');
    const _colkertas = document.querySelector('.col-kertas');
    const _colgunting = document.querySelector('.col-gunting');
    
    if(pilihanComputer == 'batu-player') return _colbatu.style.backgroundColor = '#C4C4C4';
    if(pilihanComputer == 'kertas-player') return _colkertas.style.backgroundColor = '#C4C4C4';
    if(pilihanComputer == 'gunting-player') return _colgunting.style.backgroundColor = '#C4C4C4';
   
   
})
  
});

// refresh
const _refresh = document.getElementById('_refresh');
_refresh.addEventListener('click', () =>{
    location.reload();     
});

const _back = document.getElementById('_point');
_back.addEventListener('click', () =>{
    history.back();
});